//
//  AddCardController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 06/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit
import SwaggerClient

class AddCardController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    
    private let profileImageSize = 280
    
    lazy var cardImage : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "taylor_swift_profile")
        iv.layer.cornerRadius = 10
        iv.layer.masksToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.isUserInteractionEnabled = true
        return iv
    }()
    
    lazy var editCardImageButton: UIButton = {
       let bt = UIButton()
        bt.addTarget(self, action: #selector(editImage), for: .touchUpInside)
        bt.setTitle("EDIT IMAGE", for: .normal)
        bt.setTitleColor(.red, for: .normal)
        return bt
    }()
    
    lazy var doneEditingButton: UIButton = {
        let bt = UIButton()
        bt.addTarget(self, action: #selector(doneEditing), for: .touchUpInside)
        bt.setTitle("Done editing", for: .normal)
        bt.setTitleColor(.red, for: .normal)
        return bt
    }()
    
    lazy var imagePicker : UIImagePickerController = {
        let ip = UIImagePickerController()
        ip.delegate = self
        return ip
    }()
    
    lazy var cardInterestTextView : UITextView = {
       let tv = UITextView()
        tv.text = "Please Enter your Interest Here"
        tv.textColor = .lightGray
        tv.font = UIFont(name: (tv.font?.fontName)!, size: 27)
        tv.delegate = self
        tv.textAlignment = .center
        tv.textContainer.maximumNumberOfLines = 2
        tv.layer.borderColor = UIColor.black.cgColor
        tv.layer.borderWidth = 0.5
        tv.isScrollEnabled = false
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view?.backgroundColor = .white
        self.hideKeyboardWhenTappedAround()

        
        setupPhoto()
        setupDoneButton()
        setupEditCardImageButton()
        setupCardTextView()
    }
    
    private func setupPhoto() {
        self.view?.addSubview(cardImage)
        
        let x = self.view.frame.size.width / 2 - (CGFloat(profileImageSize/2))
        
        self.view?.addConstraintsWithFormat("H:|-\(x)-[v0(\(profileImageSize))]", views: cardImage)
        self.view?.addConstraintsWithFormat("V:|-50-[v0(\(profileImageSize))]", views: cardImage)
    }
    
    private func setupDoneButton() {
        self.view?.addSubview(doneEditingButton)
        
        self.view?.addConstraintsWithFormat("H:|-30-[v0]-30-|", views: doneEditingButton)
        self.view?.addConstraintsWithFormat("V:[v0]-30-|", views: doneEditingButton)

    }
    
    private func setupCardTextView() {
        self.view?.addSubview(cardInterestTextView)
        
        self.view?.addConstraintsWithFormat("H:|-30-[v0]-30-|", views: cardInterestTextView)
        self.view?.addConstraintsWithFormat("V:[v0]-30-[v1(150)]", views: cardImage, cardInterestTextView)
    }
    
    private func setupEditCardImageButton() {
        cardImage.addSubview(editCardImageButton)
        self.view?.addConstraintsWithFormat("H:[v0]-|", views: editCardImageButton)
        self.view?.addConstraintsWithFormat("V:[v0]-|", views: editCardImageButton)
        
        
    }
    

    func doneEditing(sender: UIButton) {
        let cardSlot = Cards.sharedInstance.cards.count + 1
        
        RestApiManager.uploadCardMotto(cardSlot: Int32(cardSlot), cardMotto: cardInterestTextView.text) { (error: Error?) in
            if let e = error {
                print(e)
                print("TODO show a popup that tells user to retry uploading the card or something..")
            }
            else {
                
                if let data = UIImagePNGRepresentation(self.cardImage.image!) {
                    let filename = Utilities.getDocumentsDirectory().appendingPathComponent("card" + String(cardSlot) + ".png")
                    try? data.write(to: filename)
                    
                    
                    RestApiManager.uploadCardPhoto(cardSlot: Int32(cardSlot), cardPicturePath: filename, completion: { (url: String?, error: Error?) in
                        if let e2 = error {
                            print(e2)
                            print("TODO show a popup that tells user to retry uploading Card photo etc")
                        }
                        else {
                            let card = Card(cardSlot: cardSlot)
                            card.interest = self.cardInterestTextView.text
                            card.url = url
                            
                            CachedImageView.setImage(withKey: url! as NSString, image: self.cardImage.image!)
                            
                            Cards.sharedInstance.addCard(card: card)
                            self.navigationController?.popViewController(animated: false)
                        }
                    })
                }
            }
            
        }
    }
    
    
    func editImage(sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.cardImage.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please Enter your Interest Here"
            textView.textAlignment = .center
            textView.textColor = .lightGray
        }
        print(textView.text)
        
    }

}
