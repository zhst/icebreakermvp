//
//  HomePageViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 23/10/16.
//  Copyright © 2016 SocioLabs. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore

class HomePageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func fbLogoutButtonTap(_ sender: AnyObject) {
        
        let loginManager = LoginManager()
        loginManager.logOut()
        
        let loginPage = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageViewController") as! ViewController
        let loginPageNav = UINavigationController(rootViewController: loginPage)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = loginPageNav

    }



}
