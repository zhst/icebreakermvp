//
//  SplashScreenViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 11/02/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

class SplashScreenViewController: UIViewController, LoginButtonDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()
    }
    
    // MARK: elements
    let signupButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.setTitle("Sign up", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.5), for: UIControlState.highlighted)
        button.addTarget(self, action: #selector(signUpButtonTapped), for: .touchUpInside)
        return button
    }()
    
    let loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.setTitle("Login", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.5), for: UIControlState.highlighted)
        button.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        return button
    }()

    let alreadyUserLabel: UILabel = {
        let label = UILabel()
        label.text = "Already a user?"
        label.font = UIFont(name: (label.font?.fontName)!, size: 14)
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    
    lazy var facebookButton: LoginButton = {
        let fbButton = LoginButton(readPermissions: [.publicProfile, .email, .userFriends])
        //fbLoginBtn.readPermissions = ["public_profile", "email", "user_friends"]
        fbButton.delegate = self
        return fbButton
    }()

    // MARK: elements - GUI setup
    private func setupElements() {
        self.view.addSubview(facebookButton)        
        self.view.addSubview(signupButton)
        self.view.addSubview(alreadyUserLabel)
        self.view.addSubview(loginButton)

        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: facebookButton)
        self.view.addConstraintsWithFormat("V:|-500-[v0]", views: facebookButton)

        
        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: signupButton)
        self.view.addConstraintsWithFormat("V:[v0]-5-[v1(25)]", views: facebookButton, signupButton)

        
        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: alreadyUserLabel)
        self.view.addConstraintsWithFormat("V:[v0]-10-[v1]", views: signupButton, alreadyUserLabel)
        
        
        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: loginButton)
        self.view.addConstraintsWithFormat("V:[v0]-5-[v1(25)]", views: alreadyUserLabel, loginButton)
    }
    
    // MARK: elements - HANDLERS
    func signUpButtonTapped(sender: UIButton!) {
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "SignupStep1Id") as! SignupStep1ViewController
        self.navigationController!.show(nextViewController, sender: self)
    }
    
    func loginButtonTapped(sender: UIButton!) {
        let login = LoginViewController()
        self.navigationController!.pushViewController(login, animated: false)
        
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        switch result {
        case .failed(let error):
            print("FB ERROR", error)
        case .cancelled:
            print("User cancelled login")
        case .success(let grantedPermissions, let declinedPermissions, let accessToken):
            print("Logged in!")
            print("GRANTED PERMISSIONS: \(grantedPermissions)")
            print("DECLINED PERMISSIONS: \(declinedPermissions)")
            print("ACCESS TOKEN \(accessToken)")
            print("Token = ", accessToken.authenticationToken)
            LoadingIndicatorView.show("Registering")
            RestApiManager.facebookLogin(fbToken: accessToken.authenticationToken) {(err : Error?) in
                if let error = err {
                    print(error)
                    print("show a popup that the account is taken splash screen")
                    LoadingIndicatorView.hide()
                }
                else {
                    //TODO: do not register device if already registered.. Check return code, flag/change response
                    RestApiManager.registerDevice(p2pID: P2PManager.UUID) { (error: Error?) in
                        if let e = error {
                            print(e)
                        }
                        else {
                            LoadingIndicatorView.hide()
                            self.navigateToDiscoveryScreen()
                        }
                    }
                }
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton)
    {
        print("user is logged out")
    }
    
    
    // MARK: Helper functions
    private func navigateToDiscoveryScreen() {
        
        let layout = UICollectionViewFlowLayout()
        let homePage = DiscoveryScreenController(collectionViewLayout: layout)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.makeKeyAndVisible()
        appDelegate.window?.rootViewController = UINavigationController(rootViewController: homePage)
    }
    
    
    
    @IBAction func fastDebugTapped(_ sender: Any) {
        let layout = UICollectionViewFlowLayout()
        //        layout.scrollDirection = .horizontal  //one way to make scrolling in the collevtionView horizontal
        let homePage = DiscoveryScreenController(collectionViewLayout: layout)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.makeKeyAndVisible()
        appDelegate.window?.rootViewController = UINavigationController(rootViewController: homePage)
    }

}
