//
//  Logger.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 16/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import Foundation
import CoreLocation

enum LogLevelEnum {
    case regionEvents, regionEventsWithMessages, onlyGeneralMessages
}

class Logger {
    
    private static var logLevel: LogLevelEnum = LogLevelEnum.regionEventsWithMessages
    private static var dayTimePeriodFormatter : DateFormatter {
        let a = DateFormatter()
        a.dateFormat = "dd/MM/yyyy, HH:mm:ss:SSS"
        return a
    }
    
    // MARK: Initializer
    init() {
        Logger.dayTimePeriodFormatter.dateFormat = "dd/MM/yyyy, HH:mm:ss:SSS"
    }
    
    init(logLevel: LogLevelEnum) {
        Logger.logLevel = logLevel
        Logger.dayTimePeriodFormatter.dateFormat = "dd/MM/yyyy, HH:mm:ss:SSS"
    }
    
    // MARK: Log functions
    static func logEnterRegion(region: CLRegion) {
        Logger.printDate()
        Logger.printWrapper(items: "Entered Region: ")
        Logger.printWrapper(items: region.identifier)
        
        if let beaconRegion = region as? CLBeaconRegion {
            Logger.printWrapper(items: "UID= ")
            Logger.printWrapper(items: beaconRegion.proximityUUID)
            Logger.printWrapper(items: "Major= ")
            Logger.printWrapper(items: beaconRegion.major)
            Logger.printWrapper(items: "Minor= ")
            Logger.printWrapper(items: beaconRegion.minor)
        }
        Swift.print()
    }
    static func logExitRegion(region: CLRegion) {
        Logger.printDate()
        Logger.printWrapper(items: "Exited Region:")
        Logger.printWrapper(items: region.identifier)
        
        if let beaconRegion = region as? CLBeaconRegion {
            Logger.printWrapper(items: "UID=")
            Logger.printWrapper(items: beaconRegion.proximityUUID)
            Logger.printWrapper(items: "Major=")
            Logger.printWrapper(items: beaconRegion.major)
            Logger.printWrapper(items: "Minor=")
            Logger.printWrapper(items: beaconRegion.minor)
        }
        Swift.print()
    }
    static func logDiscovery(withBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        Logger.printDate()
        Logger.printWrapper(items: "Ranged " + String(beacons.count) + "beacons")
        Logger.printWrapper(items: "in Region")// + String(beacons.count) + "Beacons ")
        Logger.printWrapper(items: region.identifier)
        Logger.printWrapper(items: "UID=")
        Logger.printWrapper(items: region.proximityUUID)
        Logger.printWrapper(items: "Major=")
        Logger.printWrapper(items: region.major)
        Logger.printWrapper(items: "Minor=")
        Logger.printWrapper(items: region.minor)
        Logger.printWrapper(items: "!")
        
        for (index, beacon) in beacons.enumerated()
        {
            Logger.printWrapper(items: "Beacon " + String(index) + ":")
            Logger.printWrapper(items: "UID=")
            Logger.printWrapper(items: beacon.proximityUUID)
            Logger.printWrapper(items: "Major=")
            Logger.printWrapper(items: beacon.major)
            Logger.printWrapper(items: "Minor=")
            Logger.printWrapper(items: beacon.minor)
            Logger.printWrapper(items: "RSSI=")
            Logger.printWrapper(items: beacon.rssi)
            Logger.printWrapper(items: "Proximity=")
            Logger.printWrapper(items: beacon.proximity)
        }
        Swift.print()
    }
    static func logMessage(message:Any...) {
        if(Logger.logLevel == LogLevelEnum.regionEventsWithMessages)
        {
            Logger.printDate()
            Logger.printWrapper(items: message)
            Swift.print()
        }
    }
    
    static func logGeneralMessage(message:Any...) {
        Logger.printDate()
        Logger.printWrapper(items: message)
        Swift.print()
    }
    
    // MARK: Helper Functions
    private static func printDate() {
        printWrapper(items: Logger.dayTimePeriodFormatter.string(from: Date()))
    }
    private static func printWrapper(items:Any...) {
        Swift.print(items, terminator: " ")
    }
}
