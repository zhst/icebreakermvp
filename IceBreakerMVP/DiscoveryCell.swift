//
//  DiscoveryCell.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 01/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit

class DiscoveryCell: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let personCellId = "persconcell"
    
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        cv.dataSource = self
        cv.delegate = self
        
        cv.backgroundColor = UIColor.darkGray
        return cv
    }()
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = .brown
        
        addSubview(collectionView)
        
        addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        addConstraintsWithFormat("V:|[v0]|", views: collectionView)
        
        collectionView.register(PersonCell.self, forCellWithReuseIdentifier: personCellId)
        
        //add handlers to update the view
        DiscoveredPersons.sharedInstance.onCardAdded.append(reloadData)
        DiscoveredPersons.sharedInstance.onCardDeleted.append(reloadData)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DiscoveredPersons.sharedInstance.persons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: personCellId, for: indexPath) as! PersonCell
        cell.backgroundColor = UIColor.red
        
        cell.person = DiscoveredPersons.sharedInstance.persons[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 200)
    }
    
    
    
    func reloadData(person: Person) -> Void {
        collectionView.reloadData()
    }
}


