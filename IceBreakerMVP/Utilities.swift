//
//  Utilities.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 22/11/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import Foundation


class Utilities {
    
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
}
