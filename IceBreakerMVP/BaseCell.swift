//
//  BaseCell.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 04/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit


class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    func setupViews() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
