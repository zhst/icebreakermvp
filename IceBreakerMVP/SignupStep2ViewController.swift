//
//  SignupStep2ViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 15/02/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit


class SignupStep2ViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let imagePicker = UIImagePickerController()
    var email : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        setupElements()
    }
    
    // MARK: elements
    let chooseProfileLabel: UILabel = {
        let label = UILabel()
        label.text = "Select a profile picture"
        label.font = UIFont(name: (label.font?.fontName)!, size: 14)
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    lazy var photoButton: UIButton = {
        let button = UIButton()
        button.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        button.layer.borderWidth = 3
        button.setTitleColor(.red, for: .normal)
        button.clipsToBounds = true
        button.imageView!.clipsToBounds = true
        button.imageView!.contentMode = .scaleAspectFill
        button.addTarget(self, action: #selector(photoButtonTapped), for: .touchUpInside)
        return button
    }()
    
    let signupButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.isHidden = true
        button.setTitle("Break the ice", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(signUpButtonTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: elements - GUI setup
    private func setupElements() {
        self.view.addSubview(chooseProfileLabel)
        self.view.addSubview(photoButton)
        self.view.addSubview(signupButton)
        
        
        let center = self.view.frame.size.width / 2
        let photoButtonDiameter = CGFloat(200)
        let photoButtonX = center.subtracting(photoButtonDiameter.divided(by: CGFloat(2)))
        
        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: chooseProfileLabel)
        self.view.addConstraintsWithFormat("V:|-100-[v0]", views: chooseProfileLabel)

        self.view.addConstraintsWithFormat("H:|-\(photoButtonX)-[v0(\(photoButtonDiameter))]", views: photoButton)
        self.view.addConstraintsWithFormat("V:[v0]-20-[v1(\(photoButtonDiameter))]", views: chooseProfileLabel, photoButton)
        photoButton.frame.size = CGSize(width: photoButtonDiameter, height: photoButtonDiameter)
        photoButton.layer.cornerRadius = photoButton.frame.size.width/2
        photoButton.imageView!.layer.cornerRadius = photoButton.imageView!.frame.size.width / 2
        
        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: signupButton)
        self.view.addConstraintsWithFormat("V:[v0]-20-|", views: signupButton)
    }
    
    
    // MARK: elements - HANDLERS
    func signUpButtonTapped(sender: UIButton!) {
        LoadingIndicatorView.show("Registering")
        RestApiManager.register(email: email, password: nil) { (err : Error?) in
            if let error = err {
                print(error)
                print("show a popup that the account is taken")
                LoadingIndicatorView.hide()
            }
            else {
                //do CALL TO upload picture. and then
                RestApiManager.registerDevice(p2pID: P2PManager.UUID) { (error: Error?) in
                    if let e = error {
                        print(e)
                        print("show a popup that device uid could not be registered")
                        LoadingIndicatorView.hide()
                    }
                    else {
                        if let profilePicture = self.photoButton.image(for: .normal)
                        {
                            if let data = UIImagePNGRepresentation(profilePicture) {
                                let filename = Utilities.getDocumentsDirectory().appendingPathComponent("profile.png")
                                try? data.write(to: filename)
                                
                                RestApiManager.uploadProfilePicture(profilePicturePath: filename, completion: { (profilepicLocation: String?, err: Error?) in
                                    if let error = err {
                                        print(error)
                                        print("show a popup that profile photo transfer failed? or try again later?")
                                        LoadingIndicatorView.hide()
                                    }
                                    else {
                                        LoadingIndicatorView.hide()
                                        Cards.sharedInstance.profilePictureUrl = profilepicLocation
                                        Cards.sharedInstance.profilePictureSet()
                                        self.navigateToDiscoveryScreen()
                                    }
                                })
                            }
                            else {
                                LoadingIndicatorView.hide()
                                self.navigateToDiscoveryScreen()
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func photoButtonTapped(sender: UIButton!) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            photoButton.setImage(pickedImage, for: UIControlState.normal)
            photoButton.layer.borderColor = UIColor.black.cgColor
            signupButton.isHidden = false
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    private func navigateToDiscoveryScreen() {

        let layout = UICollectionViewFlowLayout()
        let homePage = DiscoveryScreenController(collectionViewLayout: layout)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.makeKeyAndVisible()
        appDelegate.window?.rootViewController = UINavigationController(rootViewController: homePage)
        
        
//        let homePage = self.storyboard?.instantiateViewController(withIdentifier: "DiscoveryScreenId") as! DiscoveryScreenController
//        let homePageNav = UINavigationController(rootViewController: homePage)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = homePageNav
    }
    
}
