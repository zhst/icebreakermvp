//
//  ProfileView.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 04/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit
import SwaggerClient


class AccountCell : BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let profileImageSize = 120
    let cardCellId = "cardcellid"
    var discoveryScreenController : DiscoveryScreenController?
    
    lazy var profilePhoto: CachedImageView = {
        let civ = CachedImageView(cornerRadius: CGFloat(self.profileImageSize / 2))
        civ.layer.backgroundColor = UIColor.brown.cgColor
        civ.layer.borderWidth = 2
        civ.layer.borderColor = UIColor.red.cgColor
        civ.backgroundColor = .green
        if (Cards.sharedInstance.profilePictureUrl != nil)
        {
            civ.loadImage(urlString: Cards.sharedInstance.profilePictureUrl!) // this is an alternative:    civ.loadProfileImage(forceLoad: false)
        }
        return civ
    }()
    
    
    
    lazy var button: UIButton = {
        let bt = UIButton()
        bt.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        bt.setTitle("ProfilePic", for: .normal)
        return bt
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        cv.dataSource = self
        cv.delegate = self
        
        cv.backgroundColor = UIColor.brown
        return cv
    }()
    
    lazy var addCardButton: UIButton = {
        let bt = UIButton()
        bt.addTarget(self, action: #selector(addCard), for: .touchUpInside)
        bt.setTitle("Add Card", for: .normal)
        return bt
    }()
    
    lazy var removeCardButton: UIButton = {
        let bt = UIButton()
        bt.addTarget(self, action: #selector(removeCard), for: .touchUpInside)
        bt.setTitle("Test button", for: .normal)
        return bt
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        fetchAccountData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupProfilePhoto()
        setupProfilePhotoButton()
        setupCardsCollectionView()
        setupAddCardButton()
        setupRemoveCardButton()
        
        
        Cards.sharedInstance.onCardAdded.append(lala)
        Cards.sharedInstance.onProfilePictureAdded = updateProfilePicture

    }
    
    
    private func fetchAccountData() {
        RestApiManager.getUserForDevice(p2pID: P2PManager.UUID) { (userSchema: UserSchema?, error: Error?)  in
            if let e = error {
                print(e)
            }
            else {
                
                guard let userSchema = userSchema else {
                    return
                }
                
                guard let serverCards = userSchema.cards else {
                    return
                }
                
                var cards : [Card] = [Card]()
                for c in serverCards {
                    let tempCard = Card(cardSlot: Int(c.cardSlot!)!)
                    tempCard.url = c.cardPictureLocation
                    tempCard.interest = c.cardMotto
                    //TODO: fetch here the image?
                    
                    cards.append(tempCard)
                }
                Cards.sharedInstance.loadCards(cards: cards)
                
                Cards.sharedInstance.profilePictureUrl = userSchema.profilePictureLocation
                Cards.sharedInstance.profilePictureSet()
            }
        }
    }

    
    private func lala(card: Card) -> Void {
        print(card.interest!)
        collectionView.reloadData()
    }
    
    private func updateProfilePicture() -> Void {
        profilePhoto.loadImage(urlString: Cards.sharedInstance.profilePictureUrl!)
    }
    
    private func setupProfilePhoto() {
        addSubview(profilePhoto)
        let x = (frame.size.width/2) - (CGFloat(profileImageSize)/2)
        addConstraintsWithFormat("H:|-\(x)-[v0(\(profileImageSize))]", views: profilePhoto)
        addConstraintsWithFormat("V:|-50-[v0(\(profileImageSize))]", views: profilePhoto)
        
    }
    
    private func setupProfilePhotoButton() {
        
        addSubview(button)
        addConstraintsWithFormat("H:|-20-[v0(100)]", views: button)
        addConstraintsWithFormat("V:|-200-[v0]", views: button)
    }
    
    private func setupAddCardButton() {
        addSubview(addCardButton)
        addConstraintsWithFormat("H:[v0]-20-[v1]", views: button, addCardButton)
        addConstraintsWithFormat("V:|-200-[v0]", views: addCardButton)
    }
    
    private func setupRemoveCardButton() {
        addSubview(removeCardButton)
        addConstraintsWithFormat("H:[v0]-20-[v1]", views: addCardButton, removeCardButton)
        addConstraintsWithFormat("V:|-200-[v0]", views: removeCardButton)
    }
    
    private func setupCardsCollectionView() {
        addSubview(collectionView)
        addConstraintsWithFormat("H:|-10-[v0]-10-|", views: collectionView)
        addConstraintsWithFormat("V:[v0]-20-[v1]-40-|", views: button, collectionView)
        collectionView.register(CardCell.self, forCellWithReuseIdentifier: cardCellId)

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Cards.sharedInstance.cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cardCellId, for: indexPath) as! CardCell
        
        cell.card = Cards.sharedInstance.cards[indexPath.item]
        
        cell.deleteCardButton.addTarget(self, action: #selector(doShit), for: .touchUpInside)
        
        return cell
    }
    
    func doShit(sender: UIButton)
    {
        
        let buttonPosition :CGPoint = sender.convert(.zero, to: self.collectionView)
        
        let b : IndexPath? = self.collectionView.indexPathForItem(at: buttonPosition)
        
        if let c: IndexPath = b! {
            print(c.item)
            let card = Cards.sharedInstance.cards[c.item]
            
            let cds = CardDeleteSchema()
            cds.cardSlot = Int32(card.cardSlot)
            
            RestApiManager.removeCard(cardDeleteSchema: cds, completion: { (error: Error?) in
                if let e2 = error {
                    print(e2)
                    print("TODO show a popup that tells user to retry uploading Card photo etc")
                }
                else {
                    Cards.sharedInstance.cards.remove(at: c.item)
                    self.collectionView.reloadData()
                }
            })
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 100)
    }

    
    
    
    func buttonAction(sender: UIButton!) {
        discoveryScreenController?.presentController()
    }
    
    func addCard(sender: UIButton!) {
        let addCardController = AddCardController()
        discoveryScreenController?.navigationController?.pushViewController(addCardController, animated:false)

//        let swimCard = Card()
//        
//        let date = Date()
//        let calendar = Calendar.current
//        let comp = calendar.dateComponents([.year, .second], from: date)
//        
//        let second = comp.second
//        
//        swimCard.interest = "Lala another card \(second!)"
//        swimCard.photo = UIImage(named: "taylor_swift_profile")
//        
//        
//        collectionView.reloadData()
//        cards.append(swimCard)
    }
    
    var text : String?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CardCell
        text = cell.cardLabel.text
        
        
        let layout = UICollectionViewFlowLayout()
        let cardDetailController = CardDetailController(collectionViewLayout: layout)
        cardDetailController.startingIndex = indexPath.item
        discoveryScreenController?.navigationController?.pushViewController(cardDetailController, animated:true)
        
    }
    
    func removeCard(sender: UIButton!) {
        print(text)

        //cards.remove(at: 1)
        
        collectionView.reloadData()

    }
    
}
