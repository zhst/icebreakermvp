//
//  DiscoveryScreenViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 18/02/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit
import SwaggerClient


class DiscoveryScreenController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
   
    let discoveryCellId = "discoverycell"
    let accountCellId = "accountcell"
    
    lazy var menuBar : MenuBar = {
        let mb = MenuBar()
        mb.discoveryScreenController = self
        return mb
    }()
    
    lazy var runOnce: Void = {
        P2PManager.shared.executeOnPeerDiscovered(self.p2pPeerDiscovered)
        P2PManager.shared.executeOnPeerLost(self.p2pPeerLost)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isTranslucent = false
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 10, height: view.frame.height))
        titleLabel.text = "ICEBREAKER"
        titleLabel.textColor = UIColor.red
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        navigationItem.titleView = titleLabel
        

        setupNavigationPermissionButton()
      
        setupCollectionView()
        setupMenuBar()
        
        DispatchQueue.main.async(execute: {
            let indexPath = IndexPath(item: 1, section: 0)
            self.menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
            self.menuBar.horizontalBarLeftAnchorConstraint!.constant = self.view.frame.width / 3
            self.collectionView?.scrollToItem(at: indexPath, at: [], animated: false)
        })
                
        _ = runOnce
    }
    
    private func setupCollectionView() {
        if let flowlayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowlayout.scrollDirection = .horizontal
            flowlayout.minimumLineSpacing = 0
        }

        
        collectionView?.backgroundColor = UIColor.white
        collectionView?.register(DiscoveryCell.self, forCellWithReuseIdentifier: discoveryCellId)
        collectionView?.register(AccountCell.self, forCellWithReuseIdentifier: accountCellId)
        
        
        collectionView?.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        
        collectionView?.isPagingEnabled = true
    }
    
       
    private func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    private func setupNavigationPermissionButton(){
        let permissionsImage = UIImage(named: "permissions_icon")?.withRenderingMode(.alwaysOriginal)
        let resizedPermissionsImage = resizeImage(image: permissionsImage!, newWidth: 30)
        let permissionButtonItem = UIBarButtonItem(image: resizedPermissionsImage, style: .plain, target: self, action: #selector(handlePermissions))
        
        
        let discoveryImage = UIImage(named: "discovery_start")?.withRenderingMode(.alwaysOriginal)
        let resizedDiscoveryImage = resizeImage(image: discoveryImage!, newWidth: 30)
        let discoveryStartButtonItem = UIBarButtonItem(image: resizedDiscoveryImage, style: .plain, target: self, action: #selector(startDiscovery))

        
        let discoveryStopImage = UIImage(named: "discovery_stop")?.withRenderingMode(.alwaysOriginal)
        let resizedDiscoveryStopImage = resizeImage(image: discoveryStopImage!, newWidth: 30)
        let discoveryStopButtonItem = UIBarButtonItem(image: resizedDiscoveryStopImage, style: .plain, target: self, action: #selector(stopDiscovery))
        

        navigationItem.rightBarButtonItems = [discoveryStopButtonItem, discoveryStartButtonItem, permissionButtonItem]
    }
    
    func handlePermissions() {
        //Discovery.sharedInstance.requestAuthorization()
        
//        PPKController.enable(withConfiguration: "898fa5f30c924574b4dfc5fd763c40bc", observer: self)
//        print(PPKController.myPeerID())
    }
    
    func startDiscovery() {
//        print("STARTINGGG DISCOVERY")
//        Discovery.sharedInstance.startWork()
    }
    
    func stopDiscovery() {
//        print("STOPPINGGG DISCOVERY")
//        Discovery.sharedInstance.stopWork()
        
        P2PManager.shared.stopP2PDiscovery()
        Cards.sharedInstance.clear()
        RestApiManager.logout()
        navigateToSplashScreen()
    }
    
    
    //////////    P2PKIT   /////////////////
    func p2pPeerDiscovered(_ peer: PPKPeer!) {
        print("Discovered peer id: ", peer.peerID)
        print("Discovered peer descr: ", peer.debugDescription)
        
        
        let p = Person()
        p.motto = peer.peerID
        p.profileImage = UIImage(named: "taylor_swift_profile")
        
        DiscoveredPersons.sharedInstance.addPerson(person: p)
    }
    
    func p2pPeerLost(_ peer: PPKPeer!) {
        print("Lost peer ", peer.peerID)

        let p = Person()
        p.motto = peer.peerID
        p.profileImage = UIImage(named: "taylor_swift_profile")
        
        DiscoveredPersons.sharedInstance.removePerson(person: p)

    }
    ////////////////////////////////////////
   
    private func setupMenuBar() {
        view.addSubview(menuBar)
        view.addConstraintsWithFormat("H:|[v0]|", views: menuBar)
        view.addConstraintsWithFormat("V:|[v0(50)]", views: menuBar)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let colors: [UIColor] = [.blue, .purple, .green]
        let cell : UICollectionViewCell

        let identifier: String
        if indexPath.item == 0 {
            identifier = accountCellId
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
            
            if let aCell = cell as? AccountCell {
                aCell.discoveryScreenController = self
            }

            
            cell.backgroundColor = colors[indexPath.item]
            

        }
        else if indexPath.item == 1 {
            identifier = discoveryCellId
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
            cell.backgroundColor = colors[indexPath.item]
            

        }
        else {
            identifier = discoveryCellId
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
            cell.backgroundColor = colors[indexPath.item]
            

        }
        
        
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset.x)
        
        menuBar.horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / 3
    }
    
    func scrollToMenuIndex(menuIndex: Int) {
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: [], animated: true)
    }
    
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let index = targetContentOffset.pointee.x / view.frame.width
        let indexPath = IndexPath(item: Int(index), section: 0)
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        
        
    }
    
    
    lazy var imagePicker : UIImagePickerController = {
        let ip = UIImagePickerController()
        ip.delegate = self
        return ip
    }()
    
    
    func presentController() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let indexPath = IndexPath(item: 0, section: 0)
        let cell = collectionView?.cellForItem(at: indexPath) as! AccountCell
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            
            if let data = UIImagePNGRepresentation(pickedImage) {
                let filename = Utilities.getDocumentsDirectory().appendingPathComponent("profilePicture.png")
                try? data.write(to: filename)
                
                RestApiManager.uploadProfilePicture(profilePicturePath: filename, completion: { (profilePicLocation: String? , error: Error?) in
                    if let err = error {
                        print(err)
                        print("show a popup that profile photo transfer failed? or try again later?")
                    }
                    else {
                        Cards.sharedInstance.profilePictureUrl = profilePicLocation
                        //cell.profilePhoto.loadImage(urlString: profilePicLocation!)
                        Cards.sharedInstance.profilePictureSet()
                        
                        //faster alternative, that does not refetch the photo:
                        //let s = profilePicLocation! as NSString
                        //cell.profilePhoto.setImage(withKey: s, image: pickedImage)
                        //cell.profilePhoto.image = pickedImage
                    }
                })
           
           }
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    private func navigateToSplashScreen() {
        let initialPage = SplashScreenViewController()
        initialPage.view.backgroundColor = .white
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.makeKeyAndVisible()
        appDelegate.window?.rootViewController = UINavigationController(rootViewController: initialPage)
    }
    
}
