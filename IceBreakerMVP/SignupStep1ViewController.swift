//
//  SignupViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 11/02/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit


class SignupStep1ViewController: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        setupElements()
        //change
    }
    
    // MARK: elements
    let nextScreenButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.layer.borderColor = UIColor.black.cgColor
        button.isHidden = true
        button.addTarget(self, action: #selector(nextScreenButtonTapped), for: .touchUpInside)
        button.setImage(UIImage(named: "rightArrow.png"), for: .normal)
        return button
    }()
    
    lazy var emailTextField: UITextField = {
        let tf = UITextField()
        tf.textColor = .black
        tf.font = UIFont(name: (tf.font?.fontName)!, size: 14)
        tf.delegate = self
        tf.textAlignment = .center
        tf.layer.borderColor = UIColor.black.cgColor
        tf.layer.borderWidth = 0
        tf.layer.cornerRadius = 5
        tf.placeholder = "Email"
//        tf.addTarget(self, action: #selector(loginFieldsDidChange(_:)), for: .editingChanged)
        tf.addTarget(self, action: #selector(emailFieldEditingDidEnd(_:)), for: .editingDidEnd)
        return tf
    }()
    
    
    private func setupElements() {
        self.view.addSubview(emailTextField)
        self.view?.addSubview(nextScreenButton)
        
        let centerY: CGFloat = view.center.y
        
        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: emailTextField)
        self.view.addConstraintsWithFormat("V:|-\(centerY)-[v0(30)]", views: emailTextField)
        
        self.view.addConstraintsWithFormat("H:[v0]-2-[v1(30)]", views: emailTextField, nextScreenButton)
        self.view.addConstraintsWithFormat("V:|-\(centerY)-[v0(30)]", views: nextScreenButton)
    }
    
    func emailFieldEditingDidEnd(_ textField: UITextField) {
        if textField.containsValidEmail() {
            self.emailTextField.layer.borderWidth = 0
            self.nextScreenButton.isHidden = false
        }
        else {
            self.emailTextField.layer.borderWidth = 0.5
            self.emailTextField.layer.borderColor = UIColor.red.cgColor
            self.nextScreenButton.isHidden = true
        }
    }
    
    func nextScreenButtonTapped(sender: UIButton) {
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "SignupStep2Id") as! SignupStep2ViewController
        nextViewController.email = emailTextField.text
        self.navigationController!.show(nextViewController, sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    

}
