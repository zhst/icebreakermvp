//
//  Person.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 22/02/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit

class Person: NSObject {
    
    var motto: String?
    var profileImage: UIImage?
    var cards: [Card]?
    
}
