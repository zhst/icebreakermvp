//
//  RegisterStep1ViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 25/10/16.
//  Copyright © 2016 SocioLabs. All rights reserved.
//

import UIKit

class RegisterStep1ViewController: UIViewController, SaveAndPopDelegate {

    
    var nextViewController : RegisterStep2ViewController!
    
    init()
    {
        super.init(nibName: nil, bundle: nil)
        nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "RegisterStep2") as! RegisterStep2ViewController
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterStep2") as! RegisterStep2ViewController
        nextViewController = vc
        nextViewController.closeHandler = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didSwipeBackwards(_ sender: UISwipeGestureRecognizer) {
        self.navigationController!.popViewController(animated: true)
    }

    @IBAction func didSwipeForward(_ sender: AnyObject) {
    
        self.navigationController!.pushViewController(nextViewController, animated: true)
        
    }

    
    func goBack(viewController : UIViewController) {
        nextViewController = viewController as! RegisterStep2ViewController
        viewController.navigationController!.popViewController(animated: true)

    }

    

}
