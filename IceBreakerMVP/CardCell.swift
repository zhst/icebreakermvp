//
//  CardCell.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 04/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit

class CardCell : BaseCell {
    
    var card: Card? {
        didSet {
            cardLabel.text = card?.interest
            
            //first check to see if the photo is loaded from server
            //else check to see if the photo was assigned through the app
            //else fallback to default image of cardCahedImageView <- currently can never happen?
            if let url = card?.url {
                cardCachedImageView.loadImage(urlString: url)  //completion: <#T##(() -> ())?##(() -> ())?##() -> ()#>)
            }
            else {
                cardCachedImageView.loadImage(urlString: "")
            }
        }
    }
    
    let cardLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Taylor Swift - Blank Space"
        return label
    }()
    
    
    let cardCachedImageView: CachedImageView = {
        let civ = CachedImageView()
        civ.layer.cornerRadius = 30
        civ.contentMode = .scaleAspectFill
        civ.layer.masksToBounds = true
        civ.image = UIImage(named: "account")
        return civ
    }()
    
    
    let deleteCardButton: UIButton = {
        let bt = UIButton()
        bt.setTitle("DEL", for: .normal)
        return bt
    }()
    
    override func setupViews() {
        
        addSubview(cardCachedImageView)
        addSubview(cardLabel)
        addSubview(deleteCardButton)
        
        addConstraintsWithFormat("H:|-16-[v0(60)]", views: cardCachedImageView)
        addConstraintsWithFormat("V:[v0(60)]", views: cardCachedImageView)

        addConstraintsWithFormat("H:[v0]-10-[v1]", views: cardCachedImageView, cardLabel)
        
        addConstraintsWithFormat("H:[v0]-10-[v1]", views: cardCachedImageView, deleteCardButton)
        addConstraintsWithFormat("V:[v0]-20-[v1]", views: cardLabel, deleteCardButton)

        //later on this shall remain and be placed in the cardImageViews location
        //addConstraintsWithFormat("H:|-106-[v0(60)]", views: cardCachedImageView)
        //addConstraintsWithFormat("V:[v0(60)]", views: cardCachedImageView)

        


    }
    
}
