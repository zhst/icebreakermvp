//
//  Card.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 04/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//
import UIKit

class Card: NSObject {
    
    var interest: String?
    var url: String?
    var cardSlot: Int
    
    init(cardSlot: Int) {
        self.cardSlot = cardSlot
    }
}
