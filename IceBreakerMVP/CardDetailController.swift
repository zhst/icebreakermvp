//
//  CardDetailViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 06/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit

class CardDetailController : UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let cardDetailId = "carddetailid"
    
    var startingIndex: Int? {
        didSet {
            navigationItem.title = "CHANGING \(startingIndex!)"
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupCollectionView()
        
        if let start = startingIndex {
            let indexPath = IndexPath(item: start, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: [], animated: false)
            
            DispatchQueue.main.async(execute: {
                let indexPath = IndexPath(item: start, section: 0)
                self.collectionView?.scrollToItem(at: indexPath, at: [], animated: false)
            })
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Cards.sharedInstance.cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cardDetailId, for: indexPath) as! CardDetailCell
        cell.card = Cards.sharedInstance.getCardForIndex(index: indexPath.item)
        
        cell.backgroundColor = .green
        
        return cell
    }

    
    
    private func setupCollectionView() {
        if let flowlayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowlayout.scrollDirection = .horizontal
            flowlayout.minimumLineSpacing = 0
        }
        
        
        collectionView?.backgroundColor = UIColor.yellow
        collectionView?.register(CardDetailCell.self, forCellWithReuseIdentifier: cardDetailId)
        
        
        collectionView?.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        
        collectionView?.isPagingEnabled = true
        
    }
    
    
    
}



class CardDetailCell : BaseCell {

    private let profileImageSize = 280

    var card : Card? {
        didSet {
            if let url = card?.url {
                cardImage.loadImage(urlString: url)  //completion: <#T##(() -> ())?##(() -> ())?##() -> ()#>)
            }
            else {
                cardImage.loadImage(urlString: "")
            }

            
            cardInterestTextView.text = card?.interest
            cardInterestTextView.font = UIFont(name: (cardInterestTextView.font?.fontName)!, size: 27)

        }
    }
    
    let cardImage: CachedImageView = {
       let civ = CachedImageView()
        civ.contentMode = .scaleAspectFill
        civ.layer.cornerRadius = 10
        civ.layer.masksToBounds = true
        civ.layer.borderWidth = 1
        civ.layer.borderColor = UIColor.white.cgColor
        return civ
    }()
    
    lazy var cardInterestTextView : UITextView = {
        let tv = UITextView()
        tv.textColor = .black
        tv.textAlignment = .center
        tv.textContainer.maximumNumberOfLines = 2
        tv.layer.borderColor = UIColor.black.cgColor
        tv.layer.borderWidth = 0.5
        tv.isScrollEnabled = false
        tv.alpha = 0.5
        return tv
    }()
    
    override func setupViews() {
        super.setupViews()
        
        
        setupPhoto()
        setupCardTextView()

    }
    
    private func setupPhoto() {
        addSubview(cardImage)
        
//        let x = frame.size.width / 2 - (CGFloat(profileImageSize/2))
//        
//        addConstraintsWithFormat("H:|-\(x)-[v0(\(profileImageSize))]", views: cardImage)
//        addConstraintsWithFormat("V:|-50-[v0(\(profileImageSize))]", views: cardImage)
        
        
        addConstraintsWithFormat("H:|-5-[v0]-5-|", views: cardImage)
        addConstraintsWithFormat("V:|-5-[v0]-5-|", views: cardImage)
    }
    
    private func setupCardTextView() {
        cardImage.addSubview(cardInterestTextView)
        
//        self.bringSubview(toFront: cardInterestTextView)
        addConstraintsWithFormat("H:|-30-[v0]-30-|", views: cardInterestTextView)
        addConstraintsWithFormat("V:[v0(150)]-80-|", views: cardInterestTextView)
    }
    
}
