//
//  RegisterStep2ViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 25/10/16.
//  Copyright © 2016 SocioLabs. All rights reserved.
//

import UIKit

class RegisterStep2ViewController: UIViewController {

    
    public var closeHandler : SaveAndPopDelegate! = nil

    @IBOutlet var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let card = CardViewController(nibName: "CardViewController", bundle: nil)
        card.view.tag = 1;
        
        
        self.addChildViewController(card)
        self.scrollView.addSubview(card.view!)
        card.didMove(toParentViewController: self)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didPanleftEdge(_ sender: UIScreenEdgePanGestureRecognizer) {

        if(sender.state == .ended)
        {
            print("go back pan")
            closeHandler.goBack(viewController: self)
        }

    }
    
    @IBAction func addCardTapped(_ sender: AnyObject) {
        let nextCard = CardViewController(nibName: "CardViewController", bundle: nil)
        
        self.addChildViewController(nextCard)
        self.scrollView.addSubview(nextCard.view!)
        nextCard.didMove(toParentViewController: self)
        
        
        let defaultViewsInScrollView = 2
        let cardCount = self.scrollView!.subviews.count - defaultViewsInScrollView
        nextCard.view.tag = cardCount;
        
        
        nextCard.view.frame.origin.x = self.view.frame.size.width * CGFloat(cardCount - 1)
        
        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width * CGFloat(cardCount), height: self.view.frame.size.height - 100)
        
        
        let rect = CGRect(origin: nextCard.view.frame.origin, size: self.scrollView.contentSize)
        self.scrollView.scrollRectToVisible(rect, animated: true)
        
        
    }
    
    
    @IBAction func removeCardTapped(_ sender: AnyObject) {
  
        //1-indexed
        let pagenumber = Int(self.scrollView.contentOffset.x / self.scrollView.bounds.size.width) + 1;
     
        var removeIndex: Int = -1
        for (index, element) in self.scrollView.subviews.enumerated()
        {
            if(element.tag == pagenumber)
            {
                removeIndex = index
                break
            }
        }
        
        
        // 1. remove card from scrollview
        let removedView = self.scrollView.subviews[removeIndex]
        removedView.removeFromSuperview()
        
        // 2. remove card from childViewController. Alternative way1 http://stackoverflow.com/questions/1372977/given-a-view-how-do-i-get-its-viewcontroller  answer with 107 Ups
        for (index, element) in self.childViewControllers.enumerated()
        {
            if(element.view.tag == pagenumber)
            {
                removeIndex = index
                break;
            }
        }
        self.childViewControllers[removeIndex].removeFromParentViewController()
        
        
        // 3. update tags and origins of the subsequent views
        for (_, element) in self.scrollView.subviews.enumerated()
        {
            if(element.tag != 0 && element.tag > pagenumber)
            {
                element.tag -= 1
                element.frame.origin.x -= self.view.frame.size.width
            }
        }
        
        
        // 4. update scrollview's size
        self.scrollView.contentSize.width -= self.view.frame.size.width
        
        
    }
    
    @IBAction func registerTapped(_ sender: AnyObject) {
                
    }

}

protocol SaveAndPopDelegate {
    
    func goBack(viewController : UIViewController)
    
}
