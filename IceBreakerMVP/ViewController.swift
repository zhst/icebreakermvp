//
//  ViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 22/10/16.
//  Copyright © 2016 SocioLabs. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore

import SwaggerClient


class ViewController: UIViewController, LoginButtonDelegate, UITextFieldDelegate {
    
    
    @IBOutlet var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let loginButton = LoginButton(readPermissions: [.publicProfile, .email, .userFriends])
        loginButton.center = view.center
        loginButton.delegate = self

        view.addSubview(loginButton)

        //fbLoginBtn.readPermissions = ["public_profile", "email", "user_friends"]
        
        nameTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(FacebookCore.AccessToken.current == nil)
        {
            print("User is not logged in")
        }
        else {
            print("User is logged in")
            navigateToHomePage()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func navigateToRegisterStep1(_ sender: AnyObject) {
        
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "RegisterStep1") as! RegisterStep1ViewController
        
 
        self.navigationController!.show(nextViewController, sender: self)
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        
        switch result {
        case .failed(let error):
            print("FB ERROR", error)
        case .cancelled:
            print("User cancelled login")
        case .success(let grantedPermissions, let declinedPermissions, let accessToken):
            print("Logged in!")
            print("GRANTED PERMISSIONS: \(grantedPermissions)")
            print("DECLINED PERMISSIONS: \(declinedPermissions)")
            print("ACCESS TOKEN \(accessToken)")
            
            
            print("Token = ", accessToken.authenticationToken)
            
           navigateToHomePage()
        }
        
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton)
    {
        print("user is logged out")
    }
    
    
    @IBAction func nativeLoginTapped(_ sender: AnyObject) {
        
        
    }
    
    
    
    private func navigateToHomePage() {
        let homePage = self.storyboard?.instantiateViewController(withIdentifier: "HomePageViewController") as! HomePageViewController
        let homePageNav = UINavigationController(rootViewController: homePage)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = homePageNav
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return nameTextField.resignFirstResponder();
    }

}

