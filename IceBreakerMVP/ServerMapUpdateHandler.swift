//
//  ServerMapUpdateHandler.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 16/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import CoreLocation

class ServerMapUpdateHandler: ServerMapUpdateDelegate {
    
    func didEnterRegion(region: CLBeaconRegion) {
        //register in server
        Logger.logGeneralMessage(message: "REGISTERING IN SERVER  " + String(describing: region))
        
        let deviceId = String(region.identifier) + "_" + String(describing: region.major!) + "_" + String(describing: region.minor!)
        if let userId = UserDefaults.standard.object(forKey: "UID") as? String
        {
//            RestApiManager.sharedInstance.registerUserAroundDevice(userId, deviceId: deviceId)
        }
        else
        {
            print("Cannot register, user not logged in, TODO : should never reach here")
            
        }
        
    }
    
    func didExitRegion(region: CLBeaconRegion) {
        //deregister in server
        Logger.logGeneralMessage(message: "DE-REGISTERING IN SERVER  " + String(describing: region))
        
        let deviceId = String(region.identifier) + "_" + String(describing: region.major!) + "_" + String(describing: region.minor!)
        if let userId = UserDefaults.standard.object(forKey: "UID") as? String
        {
//            RestApiManager.sharedInstance.deregisterUserAroundDevice(userId, deviceId: deviceId)
        }
        else
        {
            print("Cannot deregister, user not logged in, TODO : should never reach here")
        }
    }
    
    
}
