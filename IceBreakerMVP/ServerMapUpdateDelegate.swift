//
//  ServerMapUpdateDelegate.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 16/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import CoreLocation

// This is the protocol that the Delegate must implement to register/deregister the user's device on nearby devices/regions and keep the map in the server up-to-date.
protocol ServerMapUpdateDelegate {
    
    // device identified a didRangeEvent. It chose the strongest beacon. Need to register it on that device in the server map.
    func didEnterRegion(region: CLBeaconRegion)
    
    // device identified an exitRegionEvent (from specific-region(. Need to deregister it from that device in the server map.
    func didExitRegion(region: CLBeaconRegion)
    
    
    //        if let statusesArray = try? NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? [[String: AnyObject]],
    //            let user = statusesArray[0]["user"] as? [String: AnyObject],
    //            let username = user["name"] as? String {
    //                // Finally we got the username
    //        }
    //
    //        if let JSONObject = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? [[String: AnyObject]],
    //            let username = (JSONObject[0]["user"] as? [String: AnyObject])?["name"] as? String {
    //                // There's our username
    //        }
}
