//
//  CachedImageView.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 31/07/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//


import UIKit
import SwaggerClient

/**
 A convenient UIImageView to load and cache images.
 */
open class CachedImageView: UIImageView {
    
    open static let imageCache = NSCache<NSString, DiscardableImageCacheItem>()
    
    open var shouldUseEmptyImage = true
    
    private var urlStringForChecking: String?
    private var emptyImage: UIImage?
    
    public convenience init(cornerRadius: CGFloat = 0, tapCallback: @escaping (() ->())) {
        self.init(cornerRadius: cornerRadius, emptyImage: nil)
        self.tapCallback = tapCallback
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
    }
    
    func handleTap() {
        tapCallback?()
    }
    
    private var tapCallback: (() -> ())?
    
    public init(cornerRadius: CGFloat = 0, emptyImage: UIImage? = nil) {
        super.init(frame: .zero)
        contentMode = .scaleAspectFill
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        self.emptyImage = emptyImage
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     Easily load an image from a URL string and cache it to reduce network overhead later.
     
     - parameter urlString: The url location of your image, usually on a remote server somewhere.
     - parameter completion: Optionally execute some task after the image download completes
     */
    
    open func loadImage(urlString: String, completion: (() -> ())? = nil) {
        image = nil
        
        self.urlStringForChecking = urlString
        
        let urlKey = urlString as NSString
        
        if let cachedItem = CachedImageView.imageCache.object(forKey: urlKey) {
            image = cachedItem.image
            completion?()
            return
        }
        
        guard let url = URL(string: urlString) else {
            if shouldUseEmptyImage {
                image = emptyImage
            }
            return
        }
        URLSession.shared.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            if error != nil {
                return
            }
            
            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    let cacheItem = DiscardableImageCacheItem(image: image)
                    CachedImageView.imageCache.setObject(cacheItem, forKey: urlKey)
                    
                    if urlString == self?.urlStringForChecking {
                        self?.image = image
                        completion?()
                    }
                }
            }
            
        }).resume()
    }
    
    
    open static func setImage(withKey key: NSString, image: UIImage) {
        let cacheItem = DiscardableImageCacheItem(image: image)
        CachedImageView.imageCache.setObject(cacheItem, forKey: key)
    }
    
    
    
    open func loadProfileImage(forceLoad: Bool, completion: (() -> ())? = nil) {
        image = nil
        RestApiManager.getUserForDevice(p2pID: P2PManager.UUID) { (userSchema: UserSchema?, error: Error?)  in
            if let e = error {
                print(e)
            }
            else {
                
                guard let userSchema = userSchema else {
                    if self.shouldUseEmptyImage {
                        self.image = self.emptyImage
                    }
                    return
                }
                
                guard let urlKey = userSchema.profilePictureLocation else {
                    if self.shouldUseEmptyImage {
                        self.image = self.emptyImage
                    }
                    return
                }
                
                if let cachedItem = CachedImageView.imageCache.object(forKey: urlKey as NSString) {
                    self.image = cachedItem.image
                    completion?()
                    return
                }
                
                guard let url = URL(string: urlKey) else {
                    if self.shouldUseEmptyImage {
                        self.image = self.emptyImage
                    }
                    return
                }
                
                
                URLSession.shared.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
                    if error != nil {
                        return
                    }
                    
                    DispatchQueue.main.async {
                        if let image = UIImage(data: data!) {
                            let cacheItem = DiscardableImageCacheItem(image: image)
                            CachedImageView.imageCache.setObject(cacheItem, forKey: urlKey as NSString)
                            
                            self?.image = image
                            completion?()
                        }
                    }
                    
                }).resume()
            }
        }
    }
}
