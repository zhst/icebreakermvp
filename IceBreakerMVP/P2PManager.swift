//
//  P2PManager.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 30/07/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import Foundation


final class P2PManager: NSObject, PPKControllerDelegate {
    
    // 1. PPKController.enable(withConfiguration:, observer: )   should only be run once. (if already enabled a second call throws Exception)
    // 2. PPKController.enable constitues a P2PKit Usage event.
    // 3. PPKControllerInitialized/PPKControllerFailedWithError callbacks after it's executrion
    
    //TODO: state restoration : yes.
    //TODO: if BLE is closed, startP2PDiscovery() will not work. So we have to let the user know.. Also we have to call again this function, if enables it (listeners/ enable/disable)
    
    static let UUID = NSUUID().uuidString // TODO: Bad idea if the same uuidString is printed for all Apps. A hacker could guess we use this method, and then know our device id..
    
    static let p2pConfiguration = "898fa5f30c924574b4dfc5fd763c40bc"
    
    static let shared = P2PManager()
    
    var peerDiscoveredClosures : [(PPKPeer)->Void] = []
    var peerLostClosures : [(PPKPeer)->Void] = []

    
    // Can't init, is singleton
    private override init() {
        super.init()
        enableP2P()
    }
    
    
    // functions
    private func enableP2P() {
        if !PPKController.isEnabled() {
            PPKController.enable(withConfiguration: P2PManager.p2pConfiguration, observer: self)
        }
    }
    
    func startP2PDiscovery() -> Void {
        if PPKController.isEnabled() {
            
            let discoveryState = PPKController.p2pDiscoveryState()
            switch discoveryState {
            case .stopped:
                let a : Data! = Data(base64Encoded: "hi")
                PPKController.startP2PDiscovery(withDiscoveryInfo: a, stateRestoration: false) //TODO: MAKE TO true AND ADAPT PROGRAM
                print("Started p2pDiscovery")
            case .running:
                print("P2pDiscovery already running")
            case .suspended:
                // TODO: prompt to user to enable bluetooth
                print("Discovery is temporarily suspended. The discovery engine will try to restart as soon as possible (e.g. after the user did re-enable BLE)")
            case .unauthorized:
                // TODO: prompt to user give correct permissions
                print("Discovery is not able to run because of a missing user permission.")
            case .unsupported:
                // TODO: we should not end up here
                print("Discovery is not supported on this device (e.g. BLE is not available).")
            }
        }
    }
    
    func stopP2PDiscovery() {
        PPKController.stopP2PDiscovery()
    }
    
    func terminateP2P() {
        PPKController.stopP2PDiscovery()
        PPKController.disable()
    }
    
    func p2pDiscoveryState() -> PPKPeer2PeerDiscoveryState {
        return PPKController.p2pDiscoveryState()
    }
    
    
    func executeOnPeerDiscovered(_ closure: @escaping (PPKPeer)->Void) {
        peerDiscoveredClosures.append(closure)
        
    }
    func executeOnPeerLost(_ closure: @escaping (PPKPeer)->Void) {
        peerLostClosures.append(closure)
        
    }
    
    // delegates of PPK Controller
    func ppkControllerInitialized() {
        print("P2P succesfully initialized")
        startP2PDiscovery()
    }
    
    func ppkControllerFailedWithError(_ error: Error!) {
        print("P2P failed initialization")
        if let err = error as? PPKErrorCode {
            switch(err) {
            case .appKeyInvalid:
                print("App key invalid")
            case .invalidBundleId:
                print("Bundle key invalid")
            case .onlineProtocolVersionNotSupported:
                print("online protocol version not supported, update p2p framework")
            default:
                print("some other error occured: ", err)
            }
        }
    }
    
    func p2pPeerDiscovered(_ peer: PPKPeer!) {
        print("Discovered peer id: ", peer.peerID)
        print("Discovered peer descr: ", peer.debugDescription)
        
        for f in peerDiscoveredClosures {
            f(peer)
        }
    }
    
    func p2pPeerLost(_ peer: PPKPeer!) {
        print("Lost peer ", peer.peerID)
        
        
        for f in peerLostClosures {
            f(peer)
        }
    }
    
    
    
}
