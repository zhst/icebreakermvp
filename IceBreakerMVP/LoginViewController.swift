//
//  LoginViewController.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 02/04/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit
import SwaggerClient
import Alamofire


class LoginViewController: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.hideKeyboardWhenTappedAround()
        setupElements()
    }
    
    // MARK: properties
    var validEmail: Bool = false {
        didSet {
            print("hello")
            if validEmail && validPassword {
                self.loginButton.isHidden = false
            }
            else {
                self.loginButton.isHidden = true
            }
        }
    }
    
    var validPassword: Bool = false {
        didSet {
            print("hello")
            if validEmail && validPassword {
                self.loginButton.isHidden = false
            }
            else {
                self.loginButton.isHidden = true
            }
            
        }
    }
    
    // MARK: elements
    lazy var emailTextField: UITextField = {
        let tf = UITextField()
        tf.textColor = .black
        tf.font = UIFont(name: (tf.font?.fontName)!, size: 14)
        tf.delegate = self
        tf.textAlignment = .center
        tf.layer.borderColor = UIColor.black.cgColor
        tf.layer.borderWidth = 0
        tf.layer.cornerRadius = 5
        tf.placeholder = "Email"
        tf.addTarget(self, action: #selector(loginFieldsDidChange(_:)), for: .editingChanged)
        tf.addTarget(self, action: #selector(emailFieldEditingDidEnd(_:)), for: .editingDidEnd)

        return tf
    }()
    
    lazy var passwordTextField: UITextField = {
        let tf = UITextField()
        tf.textColor = .black
        tf.font = UIFont(name: (tf.font?.fontName)!, size: 14)
        tf.delegate = self
        tf.textAlignment = .center
        tf.layer.borderColor = UIColor.black.cgColor
        tf.layer.borderWidth = 0
        tf.layer.cornerRadius = 5
        tf.placeholder = "Password"
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(loginFieldsDidChange(_:)), for: .editingChanged)
        tf.addTarget(self, action: #selector(passwordFieldEditingDidEnd(_:)), for: .editingDidEnd)
        return tf
    }()

    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.setTitle("Log In", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.5), for: UIControlState.highlighted)
        button.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    // MARK: elements - GUI setup
    private func setupElements() {
        self.view.addSubview(emailTextField)
        self.view?.addSubview(passwordTextField)
        self.view?.addSubview(loginButton)
        
        let centerY = view.center.y

        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: emailTextField)
        self.view.addConstraintsWithFormat("V:|-\(centerY)-[v0(25)]", views: emailTextField)

        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: passwordTextField)
        self.view.addConstraintsWithFormat("V:[v0]-5-[v1(25)]", views: emailTextField, passwordTextField)
        
        
        self.view.addConstraintsWithFormat("H:|-50-[v0]-50-|", views: loginButton)
        self.view.addConstraintsWithFormat("V:[v0]-20-|", views: loginButton)

    }
    
    func loginTapped(sender: UIButton) {
        
        self.dismissKeyboard()
        
        LoadingIndicatorView.show("logging in..")
        
        if let email = emailTextField.text, let pass = passwordTextField.text {
            RestApiManager.login(email: email, password: pass, completion: { (err : Error?) in
        
                LoadingIndicatorView.hide()
                if let error = err as? ErrorResponse {
                    switch(error) {
                    case .Error(let errorCode, let data, let aefError):
                        print(errorCode)
                        print(data!)
//                        if let e = aefError as? Alamofire.AFError {
//                            print(e.responseCode)
//                            print(e.errorDescription)
//                        }
                        if errorCode == 401 {
                            self.emailTextField.textColor = .red
                            self.passwordTextField.textColor = .red
                        }
                    }
                }
                else {
                    self.navigateToHome()
                }
            })
        }
    }
    
    func passwordFieldEditingDidEnd(_ textField: UITextField) {
        guard let emptyPassword = textField.text?.isEmpty else {
            self.validPassword = false
            return
        }
        
        if emptyPassword {
            self.passwordTextField.layer.borderWidth = 0.5
            self.passwordTextField.layer.borderColor = UIColor.red.cgColor
            self.validPassword = false
        }
        else {
            self.passwordTextField.layer.borderWidth = 0
            self.validPassword = true
        }
    }
    
    
    func emailFieldEditingDidEnd(_ textField: UITextField) {
        if textField.containsValidEmail() {
            self.emailTextField.layer.borderWidth = 0
            self.validEmail = true
        }
        else {
            self.emailTextField.layer.borderWidth = 0.5
            self.emailTextField.layer.borderColor = UIColor.red.cgColor
            self.validEmail = false
        }
    }
    
    func loginFieldsDidChange(_ textField: UITextField) {
        emailTextField.textColor = .black
        passwordTextField.textColor = .black
    }
    
    private func navigateToHome() {
        let layout = UICollectionViewFlowLayout()
        let homePage = DiscoveryScreenController(collectionViewLayout: layout)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.makeKeyAndVisible()
        appDelegate.window?.rootViewController = UINavigationController(rootViewController: homePage)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
}
