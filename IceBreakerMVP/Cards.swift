//
//  Cards.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 06/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit

class Cards: NSObject {
    
    static let sharedInstance = Cards()
    
    var cards: [Card]
    var profilePictureUrl: String?   //TODO rename the whole Cards model class -> Data {   cards, profilePictureUrl, etc.. } //TODO why getter fails
        
    typealias cardFunc = (Card) -> Void
    var onCardAdded: [cardFunc] = []
    var onProfilePictureAdded: (Void) -> Void = {}
    
    
    private override init() {
       cards = {
            return []
        }()
    }
    
    func getCardForIndex(index: Int) -> Card? {

        if(index <= cards.count) {
            return cards[index]
        }
        else {
            return nil
        }
        
    }

    
    func loadCards(cards: [Card]) {
        for c in cards {
            addCard(card: c)
        }
    }
    
    
    func addCard(card: Card){
        cards.append(card)
        for lala in onCardAdded {
            lala(card)
        }
    }
    
    func clear() {
        cards.removeAll()
        onCardAdded.removeAll()
        onProfilePictureAdded = {}
    }
    
    
    
    // TODO put automatically this function in the setter of profilePictureUrl....
    func profilePictureSet() {
        onProfilePictureAdded()
    }
    
}
