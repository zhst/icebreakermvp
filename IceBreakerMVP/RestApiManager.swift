//
//  RestApiManager.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 05/11/16.
//  Copyright © 2016 SocioLabs. All rights reserved.
//

import Foundation
import SwiftyJSON
import FBSDKLoginKit

//
//  RestApiManager.swift
//  IceBreakerDiscovery
//
//  Created by Andreas Papageorgiou on 28/05/16.
//  Copyright © 2016 SocioLabs. All rights reserved.
//

import SwiftyJSON
import SwaggerClient

typealias ServiceResponse = (JSON, NSError?) -> Void

class RestApiManager: NSObject {
    
    static func login(email: String, password: String, completion: @escaping((_ error: Error?) -> Void)) {
        let data = LoginIBSchema()
        data.email = email
        data.password = password

        DefaultAPI.loginIbPost(loginIBData: data) { (token: String?, error: Error?) in
            if let e = error {
                print(e)
            }
            else {
                addTokenToHeaders(token: token)
            }
            completion(error)
        }
    }
    
    static func facebookLogin(fbToken: String?, completion: @escaping((_ error: Error?) -> Void)) {
        let fbData = FacebookConnectSchema()
        fbData.fbToken = fbToken
        
        DefaultAPI.connectFacebookPost(facebookConnectData: fbData) { (token: String?, error: Error?) in
            if let e = error {
                print(e)
            }
            else {
                addTokenToHeaders(token: token)
            }
            completion(error)
        }
        
    }
    
    static func register(email: String, password: String?, completion: @escaping((_ error: Error?) -> Void)) {
        // add to headers the session id for next usages
        let registerData = RegisterIBSchema()
        registerData.email = email
        registerData.password = password
        
        DefaultAPI.usersRegisterPost(registerIBData: registerData) { (token: String?, error: Error?) in
            if let e = error {
                print(e)
            }
            else {
                addTokenToHeaders(token: token)
            }
            completion(error)
        }
        
       // DefaultAPI.usersRegisterPost(registerIBData: registerData, completion: completion)
    }
    
    static func registerDevice(p2pID: String, completion: @escaping((_ error: Error?) -> Void)) {
        let devicesData = DevicesSchema()
        devicesData.p2puid = p2pID
        
        DefaultAPI.usersDevicesPut(devicesData: devicesData, completion: completion)
    }

    static func deregisterDevice(completion: @escaping((_ error: Error?) -> Void)) {
        DefaultAPI.usersDevicesDelete(completion: completion)
    }
    
    static func uploadProfilePicture(profilePicturePath: URL, completion: @escaping((_ profilePicLocation:String?, _ error: Error?) -> Void)) {
        DefaultAPI.usersProfilepicturePut(profilePhoto: profilePicturePath) { (url: String?, err: Error?) in
            if let u = url {
                //removing stupid trailing chars 
                let start = u.index(u.startIndex, offsetBy: 1)
                let end = u.index(u.endIndex, offsetBy: -2)
                let range = start..<end
                
                let urlWithoutQuotes = u.substring(with: range)
                completion(urlWithoutQuotes, err)
            }
            else {
                //TODO should never enter here, change yaml to enforce that server always returns a value here, and generator does not have an optional
                completion(url, err)
            }
        }
    }

    static func deleteProfilePicture(completion: @escaping((_ error: Error?) -> Void)) {
        DefaultAPI.usersProfilepictureDelete(completion: completion)
    }
    
    static func getUserForDevice(p2pID: String, completion: @escaping((_: UserSchema?, _ error: Error?) -> Void)) {
        DefaultAPI.usersP2pIDGet(p2pID: p2pID, completion: completion)
    }
    
    
    static func uploadCardPhoto(cardSlot: Int32, cardPicturePath: URL?, completion: @escaping((_: String?, _ error: Error?) -> Void)) {
        DefaultAPI.usersCardsPhotoPut(cardPhoto: cardPicturePath, cardSlot: cardSlot)  { (url: String?, err: Error?) in
            if let u = url {
                //removing stupid trailing chars
                let start = u.index(u.startIndex, offsetBy: 1)
                let end = u.index(u.endIndex, offsetBy: -2)
                let range = start..<end
                
                let urlWithoutQuotes = u.substring(with: range)
                completion(urlWithoutQuotes, err)
            }
            else {
                //TODO should never enter here, change yaml to enforce that server always returns a value here, and generator does not have an optional
                completion(url, err)
            }
        }

    }
    
    static func removeCardPhoto(cardSlot: Int32, completion: @escaping((_ error: Error?) -> Void)) {
        DefaultAPI.usersCardsPhotoDelete(cardSlot: cardSlot, completion: completion)
    }
    
    static func uploadCardMotto(cardSlot: Int32, cardMotto: String?, completion: @escaping((_ error: Error?) -> Void)) {
        let cardMottoData = CardMottoSchema()
        cardMottoData.cardMotto = cardMotto
        cardMottoData.cardSlot = cardSlot
        
        DefaultAPI.usersCardsMottoPut(cardMottoData: cardMottoData, completion: completion)
    }
    
    static func removeCardMotto(cardSlot: Int32, completion: @escaping((_ error: Error?) -> Void)) {
        DefaultAPI.usersCardsMottoDelete(cardSlot: cardSlot, completion: completion)
    }
    
    static func removeCard(cardDeleteSchema: CardDeleteSchema, completion: @escaping((_ error: Error?)->Void)) {
        DefaultAPI.usersCardsDelete(cardSlot: cardDeleteSchema, completion: completion)
    }
    
    static func updateAccountData(email: String?, oldPassword: String?, newPassword: String?, completion: @escaping((_ error: Error?)->Void)) {
        let accountSchema = AccountSchema()
        accountSchema.email = email
        accountSchema.oldPassword = oldPassword
        accountSchema.newPassword = newPassword
        DefaultAPI.usersAccountPut(accountData: accountSchema, completion: completion)
    }
    
    static func logout() {
        SwaggerClientAPI.customHeaders.removeValue(forKey: "Authorization")
        
        // Also logout from facebook if logged in
        if FBSDKAccessToken.current() != nil {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
        }
    }
    
    private static func addTokenToHeaders(token: String?) {
        guard let ibToken = token else {
            print("ibToken should have been returned but it's not")
            return
        }
        
        let json = JSON(data: ibToken.data(using: String.Encoding.utf8)!)
        if let t = json["ibToken"].string {
            SwaggerClientAPI.customHeaders["Authorization"] = t
        }
    }
}
