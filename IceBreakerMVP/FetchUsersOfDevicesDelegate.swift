//
//  FetchUsersOfDevicesDelegate.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 16/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

//seperate Delegates for fetching data of profiles of people registered next to devices and, reg/dereg in regions so that others can see us and keep the server-Map up to date (see ServerMapUpdateDelegate)
protocol FetchUsersOfDevicesDelegate {
    
    // Seems a good choice now to put all logic on which data we will fetch from server in the lower Discovery class level. and not let the delegate implement this logic
    // func didChangeDeviceEnvironment()
    
    // Possibility in the future: we might want to register/deregister to many devices? (hard/when to deregister etc.. TTL flag on Server?, only register in multiples?? etc.. for now MVP
    // func fetchData(FromDevices: [CLBeaconRegion], AndRegisterToNewlyFoundDevices: [CLBeaconRegion], AndDeregisterFromPreviouslyRemovedDevices: [CLBeaconRegion])
    
    // In the future maybe we need to seperate what data we request, depending on whether we are on BG mode (so get only thumbnail pic, or just username, and FG where we get everything
    // func didRequestUserDataForDevices([CLBeaconRegion] , BackgroundModeFlag)
    
    // MVP (simplest version for the moment)
    func didRequestUserData(regionUID: String, userDeviceIDs: [UInt32])
}
