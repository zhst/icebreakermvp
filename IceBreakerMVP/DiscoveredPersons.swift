//
//  DiscoveredPeers.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 28/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit

class DiscoveredPersons: NSObject {
    
    static let sharedInstance = DiscoveredPersons()
    
    var persons: [Person]
    
    typealias personFunc = (Person) -> Void
    var onCardAdded: [personFunc] = []
    var onCardDeleted: [personFunc] = []
    
    
    private override init() {
        
        persons = {
            let andyPerson = Person()
            andyPerson.motto = "Talk to me girl"
            andyPerson.profileImage = UIImage(named: "taylor_swift_profile")
            
            return [andyPerson]
        }()
    }
    
    
    func getPersonForIndex(index: Int) -> Person? {
        
        if(index <= persons.count) {
            return persons[index]
        }
        else {
            return nil
        }
        
    }
    
    func addPerson(person: Person){
        persons.append(person)
        
        //call any registered function callbacks
        for f in onCardAdded {
            f(person)
        }
    }
    
    func removePerson(person: Person) {
        
        //remove item
        var removeIdx : Int?
        for (index, val) in persons.enumerated() {
            if val.motto == person.motto
            {
                removeIdx = index
                break
            }
        }
        
        if let idx = removeIdx {
            persons.remove(at: idx)
        }


        //call any registered function callbacks
        for f in onCardDeleted {
            f(person)
        }
    }
    
}
