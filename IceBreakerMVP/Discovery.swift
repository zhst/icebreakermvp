//
//  Discovery.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 16/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//



import Foundation
import CoreLocation
import UIKit

struct Constants {
    struct DiscoveryAlgorithm {
        static let REQUEST_USER_DATA_INTERVAL = 10.0
        static let REQUEST_USER_DATA_TOLERANCE = REQUEST_USER_DATA_INTERVAL * 0.2 //20% tolerance
        
        static let CHECK_NEARBY_USERS_INTERVAL = 5.0 //has to be bigger than REQUEST_USER_DATA_INTERVAL. could be Value=30.0
        static let REMOVE_INACTIVE_USER_INTERVAL = 15.0 //has to be bigger than REQUEST_USER_DATA_INTERVAL. could be Value = 10.0
    }
    
}

struct ObserverHandlers {
    static let didEnterBackground = "didEnterBackground"
    static let didBecomeActive = "didBecomeActive"
    static let willTerminate = "willTerminate"
    static let willEnterForeground = "willEnterForeground"
}

enum MonitorStatusEnum {
    case specificMonitor, generalMonitor
}

class Discovery: NSObject {
    //MARK: Constants
    private static let USER_REGIONS_LIMIT = 14   //user regions. Saving 6 for POI_REGIONS
    
    private static let uuid = NSUUID.init(uuidString: "2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6")!
    private static let serverMapHandler = ServerMapUpdateHandler();
    private static let userDataHandler = FetchUserDataHandler();
    
    static let sharedInstance = Discovery(withUserRegions: [CLBeaconRegion.init(proximityUUID: UUID.init(uuidString: "2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6")!, identifier: "r")], withMapHandler: serverMapHandler, andDataHandler: userDataHandler)
    
    //Internal class to store the monitored userRegion and its status
    internal class UserRegion {
        private(set) internal var beaconRegion: CLBeaconRegion
        private(set) internal var status: MonitorStatusEnum
        
        init(region:CLBeaconRegion) {
            beaconRegion = region
            self.status = MonitorStatusEnum.generalMonitor
        }
        
        func monitorGeneral() {
            status = MonitorStatusEnum.generalMonitor
            beaconRegion = UserRegion.createGeneralRegion(region: beaconRegion)
        }
        
        func monitorSpecific(region: CLBeaconRegion) {
            status = MonitorStatusEnum.specificMonitor
            beaconRegion = region
        }
        
        func getGeneralRegion() -> CLBeaconRegion {
            return CLBeaconRegion.init(proximityUUID: beaconRegion.proximityUUID, identifier: getRegionIdentifier())
        }
        
        func getRegionIdentifier() -> String {
            return self.beaconRegion.identifier
        }
        
        func getRegionUID() -> String {
            return beaconRegion.proximityUUID.uuidString
        }
        
        private static func createGeneralRegion(region: CLBeaconRegion) -> CLBeaconRegion {
            return CLBeaconRegion.init(proximityUUID: region.proximityUUID, identifier: region.identifier)
        }
    }
    
    // MARK: Delegates for server communication
    private var mapDelegate: ServerMapUpdateDelegate?
    private var dataDelegate: FetchUsersOfDevicesDelegate?
    
    
    // MARK: Properties
    private let locationManager : CLLocationManager!
    private var backgroundMode: Bool = false
    private var userRegions: [UserRegion] // DIFFERENT UID!!! IF WE KEEP SAME UID, and use Major for differentiating regions, then we have only Minor(2^16=65536) unique user ids. So not scalable for London for example. UNLESS WE DO BITWISE OPERATIONS ON MAJOR (and only check FIRST PART)
    //WHY IS IT BETTER/WORSE to do it with different UIDs, or different majors?
    
    private var rangedDevices: [String: [CLBeacon]] //initially null. Dictionary with rangedDevices for each userRegion. Ex. ["2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6": [dev1, dev2] , UUID.string: [dev3, dev4]]
    private var refreshDeviceUsers: Bool = false
    private var refreshDeviceUsersTimer : Timer = Timer()
    
    
    // MARK: Initializers
    //    override init() {
    //        self.userRegions = [UserRegion]()
    //        self.rangedDevices = [String: [CLBeacon]]()
    //        super.init()
    //        locationManager.delegate = self
    //        if isAuthorized() {
    //            Logger.logMessage("Authorization Status = \(CLLocationManager.authorizationStatus().hashValue)")
    //        }
    //        backgroundMode = false
    //        activateObservers()
    //    }
    
    //convenience init<T where T:ServerMapUpdateDelegate>(withUserRegions userRegions: [CLBeaconRegion], withMapDelegate mapHandler: T) {
    private init(withUserRegions userRegions: [CLBeaconRegion], withMapHandler mapHandler: ServerMapUpdateDelegate, andDataHandler dataHandler: FetchUsersOfDevicesDelegate) {
        
        self.userRegions = [UserRegion]()
        self.rangedDevices = [String: [CLBeacon]]()
        self.locationManager = CLLocationManager()
        super.init()
        
        refreshDeviceUsersTimer = Timer.scheduledTimer(timeInterval: Constants.DiscoveryAlgorithm.REQUEST_USER_DATA_INTERVAL , target: self, selector: #selector(Discovery.enableRefreshDeviceUsersFlag), userInfo: nil, repeats: true) //TODO use CFRunLoopTimerCreateWithHandler to use closure and not expose a public func
        refreshDeviceUsersTimer.tolerance = Constants.DiscoveryAlgorithm.REQUEST_USER_DATA_TOLERANCE
        
        locationManager.delegate = self
        if isAuthorized() {
            Logger.logMessage(message: "Authorization Status = \(CLLocationManager.authorizationStatus().hashValue)")
        }
        
        backgroundMode = false
        activateObservers()
        
        
        checkDiscoveryRegions(userRegions: userRegions)//TODO add later also poiRegions
        self.userRegions = [UserRegion]()
        self.rangedDevices = [String: [CLBeacon]]()
        
        for r in userRegions
        {
            self.userRegions.append(UserRegion.init(region: r))
            self.rangedDevices[r.identifier] = [CLBeacon]()
        }
        
        mapDelegate = mapHandler
        dataDelegate = dataHandler
    }
    
    // MARK: Discovery Algorithm Simple n' Easy
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        Logger.logEnterRegion(region: region)
        if let beaconRegion = region as? CLBeaconRegion {
            if let index = getMatchingUserRegionIndex(region: beaconRegion) {
                if(userRegions[index].status ==  MonitorStatusEnum.generalMonitor) {
                    locationManager.startRangingBeacons(in: userRegions[index].beaconRegion) //actually here, range for general (we only range for specific Regions to get an EXIT event. getting an enter region with SPECIFIC cnt happen) (as explained in the else-if below)
                    //TODO timer to disableRanging after 6 seconds
                    //TODO check accessibility.. userRegions[index].status = MonitorStatusEnum.generalMonitor
                }
                else if(userRegions[index].status ==  MonitorStatusEnum.specificMonitor) {
                    // Not allowed state.
                    // We can only enter here if we have many enter regions. First one triggers Range->StopMonitorGeneral->StartMonitorSpecific. And second one is already saved, but
                    // get's triggered after monitorSpecific has been turned on. In any case, we ignore such cases.
                    // As quality test, we can see how many times we enter in this region
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        Logger.logExitRegion(region: region)
        if let beaconRegion = region as? CLBeaconRegion {
            if let index = getMatchingUserRegionIndex(region: beaconRegion) {
                if(userRegions[index].status ==  MonitorStatusEnum.specificMonitor) {
                    stopSpecificStartGeneralMonitoringNotifyServer(regionIndex: index)
                }
                else if(userRegions[index].status == MonitorStatusEnum.generalMonitor) {
                    // Not allowed state.
                    // We can only enter here if we exit specific regions while monitoring for general is on.
                    // Scenario: //?? MonitorGeneral -> ???? TODO check in morning: Event to start Range is delivered after 5 minutes and?
                }
            }
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        Logger.logDiscovery(withBeacons: beacons, inRegion: region)
        
        if let index = getMatchingUserRegionIndex(region: region) { //if our region and not some other APP
            fetchDataAndUpdateDictionary(forNewBeacons: beacons, inRegion: userRegions[index].getRegionIdentifier())
            
            if (!beacons.isEmpty) { //List of beacons can be empty. Ranging works by throwing event every microsecond: ranged x beacons (even if x=0)
                if(userRegions[index].status == MonitorStatusEnum.generalMonitor) {
                    stopGeneralStartSpecificMonitoringNotifyServer(beacon: getStrongestBeacon(fromList: beacons), regionIndex: index)
                }
                else if(userRegions[index].status == MonitorStatusEnum.specificMonitor) {
                    // non need to send Register event to server, as we are already under one specific device
                    // what if there are other devices that trigger the event to come here? think MORNING (we dont care because we are sure we are registered next to one device.
                    // otherwise, status for this region would be generalMonitor.
                }
            }
        }
        if(backgroundMode) {
            locationManager.stopRangingBeacons(in: region)
        }
    }
    
    //extra handlers for logging
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        let regionInfo : String
        if let beaconRegion = region as? CLBeaconRegion {
            regionInfo = "Ident=" + beaconRegion.identifier + "UUID=" + String(describing: beaconRegion.proximityUUID) + " Major=" + String(describing: beaconRegion.major) + " Minor=" + String(describing: beaconRegion.major)
        }
        else {
            regionInfo = ""
        }
        Logger.logMessage(message: "Failed monitoring Region " + regionInfo + " with error \(error.localizedDescription)")
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Logger.logMessage(message: "Location manager failed with error : \(error.localizedDescription)")
    }
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        let regionInfo : String
        if let beaconRegion = region as? CLBeaconRegion {
            regionInfo = "Ident=" + beaconRegion.identifier + "UUID=" + String(describing: beaconRegion.proximityUUID) + " Major=" + String(describing: beaconRegion.major) + " Minor=" + String(describing: beaconRegion.minor)
        }
        else {
            regionInfo = ""
        }
        Logger.logMessage(message: "Started Monitoring Region " + regionInfo);
    }
    func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error) {
        let regionInfo = "Ident=" + region.identifier + "UUID=" + String(describing: region.proximityUUID) + " Major=" + String(describing: region.major) + " Minor=" + String(describing: region.minor)
        Logger.logMessage(message: "Failed ranging Region " + regionInfo + " with error \(error.localizedDescription)")
    }
    
    // MARK: Helper Functions
    private func stopGeneralStartSpecificMonitoringNotifyServer(beacon:CLBeacon, regionIndex:Int) {
        assert(userRegions[regionIndex].status == MonitorStatusEnum.generalMonitor, "Impossible that monitorState is not general at this point")
        
        let specificRegion:CLBeaconRegion = getSpecificRegion(fromBeacon: beacon, withIdentifier: regionIndex)
        
        mapDelegate?.didEnterRegion(region: specificRegion) //register in server map
        locationManager.stopMonitoring(for: userRegions[regionIndex].beaconRegion) //stop general
        userRegions[regionIndex].monitorSpecific(region: specificRegion) //FLAG = specific
        locationManager.startMonitoring(for: userRegions[regionIndex].beaconRegion) //start specific
        
    }
    private func stopSpecificStartGeneralMonitoringNotifyServer(regionIndex:Int) {
        assert(userRegions[regionIndex].status == MonitorStatusEnum.specificMonitor, "Impossible that monitorState is not specific at this point")
        
        mapDelegate?.didExitRegion(region: userRegions[regionIndex].beaconRegion) //deregister in server map
        locationManager.stopMonitoring(for: userRegions[regionIndex].beaconRegion) //stop specific
        userRegions[regionIndex].monitorGeneral() //FLAG = general
        locationManager.startMonitoring(for: userRegions[regionIndex].beaconRegion) //start general monitor
    }
    private func fetchDataAndUpdateDictionary(forNewBeacons beacons:[CLBeacon], inRegion regionID: String) {
        let currBeaconsInRegion = rangedDevices[regionID]! //since we call this method only after we have found a mathcingUserRegionIndex, we have a dictionary entry for this KEY. *even if initially it has no elements
        
        if(refreshDeviceUsers) {
            refreshDeviceUsers = false;
            let beaconList = FastCompareHelper.createSetFromBeaconRegionArray(beacons: currBeaconsInRegion)
            dataDelegate?.didRequestUserData(regionUID: regionID, userDeviceIDs: Array(beaconList))
        }
        else {
            //Raise Event Only in case we discover new beacons
            let newBeaconsList : Set<UInt32> = FastCompareHelper.getDifferenceBetween(existing: currBeaconsInRegion, andNew: beacons)
            if(!newBeaconsList.isEmpty) {
                dataDelegate?.didRequestUserData(regionUID: regionID, userDeviceIDs: Array(newBeaconsList))
            }
        }
        rangedDevices[regionID] = beacons
    }
    private func getMatchingUserRegionIndex(region : CLBeaconRegion) -> Int? {
        for (index, r) in self.userRegions.enumerated()
        {
            if(r.beaconRegion.proximityUUID == region.proximityUUID)
            {
                return index
            }
        }
        return nil
    }
    private func getSpecificRegion(fromBeacon beacon: CLBeacon, withIdentifier index: Int) -> CLBeaconRegion {
        return CLBeaconRegion(proximityUUID: beacon.proximityUUID, major: beacon.major.uint16Value, minor: beacon.minor.uint16Value, identifier: userRegions[index].getRegionIdentifier())
    }
    
    private func getStrongestBeacon(fromList beacons: [CLBeacon] ) -> CLBeacon {
        var strongestBeacon = beacons[0]
        for beacon in beacons
        {
            if (beacon.rssi > strongestBeacon.rssi)
            {
                strongestBeacon = beacon
            }
        }
        return strongestBeacon
    }
    private func startRangingAllRegions() {
        for r in userRegions
        {
            locationManager.startRangingBeacons(in: r.getGeneralRegion())
        }
    }
    private func stopRangingAllUserRegions() {
        for r in userRegions
        {
            locationManager.stopRangingBeacons(in: r.getGeneralRegion())
        }
    }
    private func stopMonitoringEverything() {
        for r in userRegions
        {
            locationManager.stopMonitoring(for: r.beaconRegion) //should be enough..
            locationManager.stopMonitoring(for: r.getGeneralRegion())
        }
    }
    
    private func checkDiscoveryRegions(userRegions: [CLBeaconRegion]) {
        //TODO [enable poiRegions, and check for them]. Also ensure that poiRegions and userRegions have different UIDs.
        checkUserRegions(regions: userRegions)
    }
    private func checkUserRegions(regions: [CLBeaconRegion]) {
        assert(regions.count <= Discovery.USER_REGIONS_LIMIT, "iOS 9.0 allows up to 20 regions to be searched")
        // TODO assert all UIDS are the same
    }
    
    private func isAuthorized() -> Bool {
        return (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways)
    }
    
    func enableRefreshDeviceUsersFlag() {
        self.refreshDeviceUsers = true;
    }
    
    
    // MARK: Application State Notification
    private func activateObservers() {
        
        NotificationCenter.default.addObserver(self, selector: Selector.init(ObserverHandlers.didEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: Selector.init(ObserverHandlers.didBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: Selector.init(ObserverHandlers.willTerminate), name: NSNotification.Name.UIApplicationWillTerminate, object: nil) //probably unnecessary
        NotificationCenter.default.addObserver(self, selector: Selector.init(ObserverHandlers.willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil) //probably unnecessary
        
        // probably not needed
        //UIApplicationDidFinishLaunchingNotification //any final initialization
        //UIApplicationWillResignActiveNotification //app transitions away from foreground in quiescient state
    }
    func didEnterBackground() {
        Logger.logMessage(message: "App entered Background")
        backgroundMode = true
        stopRangingAllUserRegions()
        refreshDeviceUsersTimer.invalidate();
    }
    internal func didBecomeActive() {
        Logger.logMessage(message: "App entered foreground")
        backgroundMode = false
        startRangingAllRegions()
        refreshDeviceUsersTimer = Timer.scheduledTimer(timeInterval: Constants.DiscoveryAlgorithm.REQUEST_USER_DATA_INTERVAL , target: self, selector: #selector(Discovery.enableRefreshDeviceUsersFlag), userInfo: nil, repeats: true)
        refreshDeviceUsersTimer.tolerance = Constants.DiscoveryAlgorithm.REQUEST_USER_DATA_TOLERANCE
    }
    
    //TODO needed?
    func willTerminate() {
        backgroundMode = true
        stopRangingAllUserRegions()
    }
    func willEnterForeground() {
        //        backgroundMode = false
    }
    
    // MARK: Methods
    
    //start/stop work not used, can be deleted. request authorization can stay
    func startWork() {
        if isAuthorized() {
            startRangingAllRegions()
        }
        else {
            Logger.logMessage(message: "App not authorized")
        }
    }
    func stopWork() {
        stopRangingAllUserRegions()
        stopMonitoringEverything()
    }
    func requestAuthorization() {
        return locationManager.requestAlwaysAuthorization()
    }
    
}


//TODO Maybe put in extension CLLocationManagerDelegate
extension Discovery: CLLocationManagerDelegate {
    // MARK: Discovery Algorithm
    
    // -> TODO 1 ALSO keep monitoring for POIs/spaces (5 regions//for overlapping ones)
    // -> TODO 2 with parameter in logging enable/disable logging ( level 0 dont print anything, level 1 print something etc etc
    // -> TODO 3 atomic functions/semaphors to access/write the backgroundMode value? / start/stop etc?
    // -> TODO 4 register 1, vs register 1 and Many with TTL
}
