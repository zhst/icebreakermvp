//
//  PersonCell.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 22/02/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//

import UIKit


class PersonCell : BaseCell {
    
    var person: Person? {
        didSet {
           mottoLabel.text = person?.motto
           userProfileImageView.image = person?.profileImage
        }
    }
    
    let userProfileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "taylor_swift_profile")
        imageView.layer.cornerRadius = 22
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let mottoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Taylor Swift - Blank Space"
        return label
    }()
    
    
    override func setupViews() {
        
        addSubview(userProfileImageView)
        addSubview(mottoLabel)

        
        addConstraintsWithFormat("H:|-16-[v0(44)]", views: userProfileImageView)
        
        
    }
    
}
