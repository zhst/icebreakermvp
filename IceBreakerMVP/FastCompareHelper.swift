//
//  FastCompareHelper.swift
//  IceBreakerMVP
//
//  Created by Andreas Papageorgiou on 16/03/2017.
//  Copyright © 2017 SocioLabs. All rights reserved.
//


import Foundation
import CoreLocation


class FastCompareHelper {
    
    // ************************************************** METHOD 1 - SETS **************************************************
    //1 get difference
    //2 receive data for output
    //3 update with newBeacons in Discovery
    class func getDifferenceBetween(existing prevList: [CLBeacon], andNew currList: [CLBeacon]) -> Set<UInt32> {
        let prevSet: Set<UInt32> = createSetFromBeaconRegionArray(beacons: prevList)
        let currSet: Set<UInt32> = createSetFromBeaconRegionArray(beacons: currList)
        
        return currSet.subtracting(prevSet)
    }
    
    public class func createSetFromBeaconRegionArray(beacons: [CLBeacon]) -> Set<UInt32> {
        var ret = Set<UInt32>()
        for b in beacons {
            ret.insert(toUInt32(value1: b.major.uint16Value, value2: b.minor.uint16Value))
        }
        return ret
    }
    
    private class func toUInt32(value1:UInt16,value2:UInt16) -> UInt32 {
        return (UInt32(value1) << 16 | UInt32(value2))
    }
    
    internal class func getMajorMinor(value: UInt32) -> (major: UInt16, minor: UInt16) {
        
        return (UInt16(value / (1<<16)), UInt16(value % (1<<16)))
        
    }
    // *********************************************************************************************************************
    
    
    
    
    
    
    
    
    
    
    
    // ********************************************** METHOD 2 - DICTIONARIES **********************************************
    
    // Find elements in New Dictionary that are not in the old.
    //    // In SETS terms:   DictionaryNew \ DictionaryOld
    //    static func subtract(dictionaryNew: [Int: Int], From dictionaryOld: [Int: Int]) -> [Int]
    //    {
    //
    //        var x =CLBeaconRegion()
    //
    //
    //        x.minor.
    //
    //        var ret : [Int] = [Int]()
    //        //Attention for == to work both Key and Value must be equatable
    //        //Attention2  Maybe unnecessary action, since we implement it as well below to get the differences
    //        if(dictionaryNew != dictionaryOld)
    //        {
    //            for (k, v) in dictionaryOld {
    //                let v [k]
    //            }
    //
    //        }
    //
    //    }
    // *********************************************************************************************************************
    
    
    
}

